<?php

/**
 * @file
 * Renders Unilogin link.
 */

?>
<?php
print l(
  '<div class="unilogin">' . theme('image', $image_path) . '</div>',
  $login_url,
  array(
    'query' => $query,
    'html' => TRUE,
    'attributes' => array('class' => 'unilogin-link'),
  )
);
?>
