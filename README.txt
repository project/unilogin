Description:
This module provides a way to authenticate a drupal user
through the external "Unilogin" service.
Unilogin is a danish SSO service that is shared between educational institutes:
http://www.uni-c.dk/produkter/support/uddannelse/uni-login/index.html

How does it work?:
  1. The module provides a block with a link to authenticated with
     the Unilogin service.
  2. The external Unilogin form redirects after submission to the drupal site
     and authenticates the user with a soap request back to Unilogin.
  3. If the request passes it logs in the drupal user automatically.

Module requirements:
PHP Soap extension with the soap client enabled.

Install instructions:
* Read Unilogin documentation.
* Fill in settings provided by Unilogin in the configuration form.
* Activate Unilogin block beneath the normal login form.

Unilogin documentation:
  About Unilogin:
  http://www.uni-c.dk/produkter/infrastruktur/brugere-index.html
  
  About Unilogin authentication:
  http://www.uni-c.dk/produkter/infrastruktur/uni-login/technicaldescriptionofuni-loginforwebbasedapplications.pdf

  About Unilogin webservice ws-02:
  http://www.uni-c.dk/produkter/infrastruktur/uni-login/infotjenestenswebservice_ws02.pdf
