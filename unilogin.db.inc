<?php

/**
 * @file
 * Db functions for the unilogin project.
 *
 * @author Mikkel Jakobsen <mikkel@adapt.dk>
 */

/**
 * Save unilogin id.
 *
 * @param integer $uid
 *   Drupal user id.
 * @param string $unilogin_id
 *   Unilogin id.
 *
 * @return void
 *   Does not return anything.
 */
function _unilogin_db_save_unilogin_id($uid, $unilogin_id) {
  if (empty($uid) || empty($unilogin_id)) {
    return;
  }
  $uni = new stdClass();
  $uni->uid = $uid;
  $uni->unilogin_id = $unilogin_id;
  $unilogin_db = _unilogin_db_load_unilogin_id_by_uid($uid);
  if (!$unilogin_db) {
    drupal_write_record('unilogin_users', $uni);
  }
  elseif ($unilogin_db != $unilogin_id) {
    drupal_write_record('unilogin_users', $uni, 'uid');
  }
}

/**
 * Delete unilogin id. Deletes relation between uid and unilogin id.
 *
 * @param integer $uid
 *   Drupal user id.
 */
function _unilogin_db_delete_unilogin_id($uid) {
  if (!empty($uid)) {
    db_query("DELETE FROM {unilogin_users} WHERE uid = %d", $uid);
  }
}

/**
 * Get user id from a unilogin id.
 *
 * @param string $unilogin_id
 *   The unilogin id of the user.
 *
 * @return mixed
 *   integer/FALSE
 */
function _unilogin_db_load_uid_by_unilogin_id($unilogin_id) {
  if (empty($unilogin_id)) {
    return;
  }
  return db_result(
    db_query(
      "SELECT uid FROM {unilogin_users} WHERE unilogin_id = '%s'",
      $unilogin_id
    )
  );
}

/**
 * Get unilogin id from user id.
 *
 * @param integer $uid
 *   Drupal user id.
 *
 * @return mixed
 *   string/FALSE
 */
function _unilogin_db_load_unilogin_id_by_uid($uid) {
  if (empty($uid)) {
    return;
  }
  return db_result(
    db_query("SELECT unilogin_id FROM {unilogin_users} WHERE uid = %d", $uid)
  );
}

/**
 * Load a ticket from db. Returns FALSE if there is no match.
 *
 * @param string $name
 *   The generated name of the ticket.
 *
 * @return mixed
 *   array/FALSE
 */
function _unilogin_db_load_ticket($name) {
  if (empty($name)) {
    return FALSE;
  }
  return db_fetch_object(
    db_query("SELECT * FROM {unilogin_tickets} WHERE name = '%s'", $name)
  );
}

/**
 * Save a ticket.
 *
 * @param string $auth
 *   Auth token.
 * @param integer $timestamp
 *   Unilogin timestamp.
 * @param string $unilogin_id
 *   Unilogin user id.
 *
 * @return void
 *   Nothing to return.
 */
function _unilogin_db_save_ticket($auth, $timestamp, $unilogin_id) {
  if (empty($auth) || empty($timestamp) || empty($unilogin_id)) {
    return;
  }
  $tname = _unilogin_construct_ticket_name($auth, $timestamp, $unilogin_id);
  $ticket = new stdClass();
  $ticket->name = $tname;
  $ticket->created = time();
  if (!_unilogin_db_load_ticket($tname)) {
    drupal_write_record('unilogin_tickets', $ticket);
  }
}
